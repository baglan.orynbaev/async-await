﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncAwait
{
    public class Program
    {
        static Random random = new Random();

        static async Task Main(string[] args)
        {
            var array = await CreateArray();
            var multipliedArray = await MultiplyArray(array, random.Next(100));
            var sortedArray =  await SortArray(multipliedArray);
            var average = await CalculateAverage (sortedArray);

            Console.WriteLine(average);
            Console.ReadLine();
        }

        public static async Task<int[]> CreateArray()
        {
            return await Task.Run(() =>
            {
                int[] arr = new int[10];
                for (int i = 0; i < 10; i++)
                {
                    arr[i] = random.Next(100);
                    Console.Write(arr[i] + ", ");
                }

                Console.WriteLine();

                return arr;
            });
        }

        public static async Task<int[]> MultiplyArray(int[] arr, int mult)
        {
            if (arr == null)
            {
                throw new ArgumentNullException(nameof(arr));
            }

            if (arr.Length == 0)
            {
                throw new ArgumentException("array cannot be empty", nameof(arr));
            }

            return await Task.Run(() =>
            {
                for (int i = 0; i < arr.Length; i++)
                {
                    arr[i] *= mult;
                    Console.Write(arr[i] + ", ");
                }

                Console.WriteLine();

                return arr;
            });
        }

        public static async Task<int[]> SortArray(int[] arr)
        {
            if (arr == null)
            {
                throw new ArgumentNullException(nameof(arr));
            }

            if (arr.Length == 0)
            {
                throw new ArgumentException("array cannot be empty", nameof(arr));
            }

            return await Task.Run(() =>
            {
                Array.Sort(arr);

                for (int i = 0; i < arr.Length; i++)
                {
                    Console.Write(arr[i] + ", ");
                }

                Console.WriteLine();

                return arr;
            });
        }

        public static async Task<int> CalculateAverage(int[] arr)
        {
            if (arr == null)
            {
                throw new ArgumentNullException(nameof(arr));
            }

            if (arr.Length == 0)
            {
                throw new ArgumentException("array cannot be empty", nameof(arr));
            }

            return await Task.Run(() =>
            {
                int sum = 0;
                for (int i = 0; i < arr.Length; i++)
                {
                    sum += arr[i];
                }

                sum /= arr.Length;

                return sum;
            });
        }
    }
}
